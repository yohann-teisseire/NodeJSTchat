(function($){
	var socket = io.connect('http://localhost:1337/');
	var msgtpl = $('#msgtpl').html();
	$('#msgtpl').remove();

	$('#loginform').submit(function(event){
		event.preventDefault();

		socket.emit('login', {
			username : $('#username').val(),
			mail : $('#mail').val()
		})
	});



	//Si on est connecté

	socket.on('logged', function(){
		$('.fadeOut').fadeOut();
		$('#message').focus();

	});

	/****** Message Send *******/

	$('#form').submit(function(event){
		event.preventDefault();
		socket.emit('newMessage', {message : $('#message').val() });
		$('#message').val('');
		$('#message').focus();
	});

	/****** Delivered Message *******/

	socket.on('newMessage', function(message){
		
		$('#chat-box').append('<div class="item">' + Mustache.render(msgtpl, message) + '</div>');
	});


	/****** Gestion des connectés ********/

	//ecouter event
	socket.on('newUser', function(user){
		$('#tchat').append('<img width="50" height="50" src="' + user.avatar + '" id="' + user.id + '">');
	});

	// si on se deconnecte

	socket.on('delUser', function(user){
		$('#' + user.id).remove();
	});

})(jQuery);