var http = require('http');

httpServer = http.createServer(function(req,res){ 
	res.end('Hello Word');
});

httpServer.listen(1337); 

var io = require('socket.io').listen(httpServer); 
var users = {}; // Objet
var messages = []; //tableau
var history = 10;

io.sockets.on('connection', function(socket){

	var me = false;
	console.log('Nouvel utilisateur');

	for (var k in users) {
		socket.emit('newUser', users[k]);
	};

	for (var y in messages) {
		socket.emit('newMessage', messages[y]);
	};

	/****** Get Message ******/

	socket.on('newMessage', function(message){
		message.user = me;
		date = new Date();
		message.h = date.getHours();
		message.m = date.getMinutes();
		messages.push(message);
		if(messages.length > history){
			messages.shift();
		}
		io.sockets.emit('newMessage', message);
	});

	/****** Login *****/

	socket.on('login', function(user){
		me = user;
		me.id = user.mail.replace('@', '-').replace('.', '-');
		me.avatar = 'http://lorempixel.com/50/50/sports/';
		socket.emit('logged');
		users[me.id] = me;
		io.sockets.emit('newUser', me);
	});

	/****** Logout *****/

	socket.on('disconnect', function(){
		if(!me){
			return false;
		}
		delete users[me.id];
		io.sockets.emit('delUser', me);
	})
});